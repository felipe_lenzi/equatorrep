#pragma once

#include <JuceHeader.h>

// Spectrum analyzer code (faulty)
/*
#include <array>
template<typename T>
struct Fifo
{
    void prepare(int numChannels, int numSamples)
    {
        static_assert(  std::is_same_v<T, juce::AudioBuffer<float>>,
                        "prepare(numChannels, numSamples) should only be used when the Fifo is holding juce::AudioBuffer<float>");

        for (auto& buffer : buffers)
        {
            buffer.setSize( numChannels,
                            numSamples,
                            false,      // clear everything?
                            true,       // including the extra steps?
                            true);      // avoid reallocating if you can?
            buffer.clear();
        }
    }

    void prepare(size_t numElements)
    {
        static_assert(  std::is_same_v<T, std::vector<float>>,
                        "prepare(numElements) should only be used when the Fifo is holding std::vector<float>");

        for (auto& buffer : buffers)
        {
            buffer.clear();
            buffer.resize(numElements, 0);
        }
    }

    bool push(const T& t)
    {
        auto write = fifo.write(1)
        if (write.blockSize1 > 0)
        {
            buffers[write.startIndex1] = t;
            return true;
        }

        return false;
    }

    bool pull(T& t)
    {
        auto read = fifo.read(1);
        if (read.blockSize1 > 0)
        {
            t = buffers[read.startIndex1];
            return true;
        }

        return false;
    }

    int getNumAvailableForReading() const
    {
        return fifo.getNumReady();
    }
private:
    static constexpr int Capacity = 30;
    std::array<T, Capacity> buffers;
    juce::AbstractFifo fifo {Capacity};
};

enum Channel
{
    Right,  // effectively 0
    Left    // effectively 1
};

template<typename BlockType>
struct SingleChannelSampleFifo
{
    SingleChannelSampleFifo(Channel ch) : channelToUse(ch)
    {
        prepared.set(false);
    }

    void update(const BlockType& buffer)
    {
        jassert(prepared.get());
        jassert(buffer.getNumChannels() > channelToUse);
        auto* channelPtr = buffer.getReadPointer(channelToUse);

        for (int i = 0; i < buffer.getNumSamples(); ++i)
        {
            pushNextSampleIntoFifo(channelPtr[i]);
        }
    }

    void prepare(int bufferSize)
    {
        prepared.set(false);
        size.set(bufferSize);

        bufferToFill.setSize(   1,              // channel
                                bufferSize,     // num samples
                                false,          // keepExistingContent
                                true,           // clear extra space
                                true);          // avoid reallocating
        audioBufferFifo.prepare(1, bufferSize);
        fifoIndex = 0;
        prepared.set(true);
    }
    //==============================================================================
    int getNumCompleteBuffersAvailable() const { return audioBufferFifo.getNumAvailableForReading(); }
    bool isPrepared() const { return prepared.get(); }
    int getSize() const { return size.get(); }
    //==============================================================================
    bool getAudioBuffer(BlockType& buf) { return audioBufferFifo.pull(buf); }
private:
    Channel channelsToUse;
    int fifoIndex = 0;
    Fifo<BlockType> audioBufferFifo;
    BlockType bufferToFill;
    juce::Atomic<bool> prepared = false;
    juce::Atomic<int> size = 0;

    void pushNextSampleIntoFifo(float sample)
    {
        if (fifoIndex == bufferToFill.getNumSamples())
        {
            auto ok = audioBufferFifo.push(bufferToFill);
            
            juce::ignoreUnused(ok);

            fifoIndex = 0;
        }

        bufferToFill.setSample(0, fifoIndex, sample);
        ++fifoIndex;
    }
};
*/

enum slope
{
    slope_12,
    slope_24,
    slope_36,
    slope_48
};

struct chainSettings
{
    float lowCutFreq{ 0 }, highCutFreq{ 0 };
    float peakFreq{ 0 }, peakGainInDb{ 0 }, peakQuality{ 1.0f };

    slope lowCutSlope{ slope::slope_12 }, highCutSlope{ slope::slope_12 };

    bool lowCutBypassed{ false }, peakBypassed{ false }, highCutBypassed{ false };
};

chainSettings getChainSettings(juce::AudioProcessorValueTreeState& apvts);

using filter = juce::dsp::IIR::Filter<float>;

// 4 filters of 12dB/Oct slope each = 1 48dB/Oct filter.
using cutFilter = juce::dsp::ProcessorChain<filter, filter, filter, filter>;

using monoChain = juce::dsp::ProcessorChain<cutFilter, filter, cutFilter>;

enum chainPositions
{
    lowCut,
    peak,
    highCut
};

using coefficients = filter::CoefficientsPtr;
void updateCoefficients(coefficients& old, const coefficients& replacements);

coefficients makePeakFilter(const chainSettings& chainSet, double sampleRate);

template<int index, typename chainType, typename coefficientType>
void update(chainType& chain, const coefficientType& coefficients)
{
    updateCoefficients(chain.template get<index>().coefficients, coefficients[index]);
    chain.template setBypassed<index>(false);
}

template<typename chainType, typename coefficientType>
void updateCutFilter(chainType& chain,
    const coefficientType& coefficients,
    const slope& slope)
{
    chain.template setBypassed<0>(true);
    chain.template setBypassed<1>(true);
    chain.template setBypassed<2>(true);
    chain.template setBypassed<3>(true);

    switch (slope)
    {
        case slope_48:
        {
            update<3>(chain, coefficients);
        }
        case slope_36:
        {
            update<2>(chain, coefficients);
        }
        case slope_24:
        {
            update<1>(chain, coefficients);
        }
        case slope_12:
        {
            update<0>(chain, coefficients);
        }
    }
}

inline auto makeLowCutFilter(const chainSettings chainSet, double sampleRate)
{
    return juce::dsp::FilterDesign<float>::
        designIIRHighpassHighOrderButterworthMethod( chainSet.lowCutFreq,
                                                     sampleRate,
                                                     (chainSet.lowCutSlope + 1) * 2);
                                                // slope choice 3: 48 dB/Oct ---> order: 8
                                                // slope choice 0: 12 dB/Oct ---> order: 2
                                                // slope choice 1: 24 dB/Oct ---> order: 4
                                                // slope choice 2: 36 dB/Oct ---> order: 6
}

inline auto makeHighCutFilter(const chainSettings chainSet, double sampleRate)
{
    return juce::dsp::FilterDesign<float>::
        designIIRLowpassHighOrderButterworthMethod(  chainSet.highCutFreq,
                                                     sampleRate,
                                                     (chainSet.highCutSlope + 1) * 2);
}

//==============================================================================

class EQuatorAudioProcessor : public juce::AudioProcessor
{
public:

    EQuatorAudioProcessor();
    ~EQuatorAudioProcessor() override;

    //==============================================================================

    void prepareToPlay(double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

    void processBlock(juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================

    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================

    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================

    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram(int index) override;
    const juce::String getProgramName(int index) override;
    void changeProgramName(int index, const juce::String& newName) override;

    //==============================================================================

    void getStateInformation(juce::MemoryBlock& destData) override;
    void setStateInformation(const void* data, int sizeInBytes) override;

    static juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();

    juce::AudioProcessorValueTreeState apvts { *this, nullptr, "Parameters", 
        createParameterLayout() };

    //using BlockType = juce::AudioBuffer<float>;
    //SingleChannelSampleFifo<BlockType> leftChannelFifo{ Channel::Left };
    //SingleChannelSampleFifo<BlockType> rightChannelFifo{ Channel::Right };

private:

    // Stereo chain.
    monoChain leftChain, rightChain;

    void updatePeakFilter(const chainSettings& chainSet);

    void updateLowCutFilters(const chainSettings& chainSet);
    void updateHighCutFilters(const chainSettings& chainSet);

    void updateFilters();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (EQuatorAudioProcessor)
};
