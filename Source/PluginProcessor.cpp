#include "PluginProcessor.h"
#include "PluginEditor.h"


chainSettings getChainSettings(juce::AudioProcessorValueTreeState& apvts)
{
    chainSettings settings;

    settings.lowCutFreq = apvts.getRawParameterValue("LOWCUT FREQ")->load();
    settings.highCutFreq = apvts.getRawParameterValue("HIGHCUT FREQ")->load();
    settings.peakFreq = apvts.getRawParameterValue("PEAK FREQ")->load();
    settings.peakGainInDb = apvts.getRawParameterValue("PEAK GAIN")->load();
    settings.peakQuality = apvts.getRawParameterValue("PEAK QUALITY")->load();
    settings.lowCutSlope = static_cast<slope>(apvts.getRawParameterValue("LOWCUT SLOPE")->load());
    settings.highCutSlope = static_cast<slope>(apvts.getRawParameterValue("HIGHCUT SLOPE")->load());

    settings.lowCutBypassed = apvts.getRawParameterValue("LOWCUT BYPASS")->load() > 0.5f;
    settings.peakBypassed = apvts.getRawParameterValue("PEAK BYPASS")->load() > 0.5f;
    settings.highCutBypassed = apvts.getRawParameterValue("HIGHCUT BYPASS")->load() > 0.5f;

    return settings;
};

coefficients makePeakFilter(const chainSettings& chainSet, double sampleRate)
{
    return juce::dsp::IIR::Coefficients<float>::makePeakFilter (sampleRate, 
                                                                chainSet.peakFreq, 
                                                                chainSet.peakQuality,
                                                                juce::Decibels::decibelsToGain(chainSet.peakGainInDb));
}

void EQuatorAudioProcessor::updatePeakFilter(const chainSettings& chainSet)
{
    auto peakCoefficients = makePeakFilter(chainSet, getSampleRate());

    leftChain.setBypassed<chainPositions::peak>(chainSet.peakBypassed);
    rightChain.setBypassed<chainPositions::peak>(chainSet.peakBypassed);

    updateCoefficients(leftChain.get<chainPositions::peak>().coefficients, peakCoefficients);
    updateCoefficients(rightChain.get<chainPositions::peak>().coefficients, peakCoefficients);
}

void updateCoefficients(coefficients& old, const coefficients& replacements)
{
    *old = *replacements;
}

void EQuatorAudioProcessor::updateLowCutFilters(const chainSettings& chainSet)
{
    auto cutCoefficients = makeLowCutFilter(chainSet, getSampleRate());

    auto& leftLowCut = leftChain.get<chainPositions::lowCut>();
    auto& rightLowCut = rightChain.get<chainPositions::lowCut>();

    leftChain.setBypassed<chainPositions::lowCut>(chainSet.lowCutBypassed);
    rightChain.setBypassed<chainPositions::lowCut>(chainSet.lowCutBypassed);

    updateCutFilter(leftLowCut, cutCoefficients, chainSet.lowCutSlope);
    updateCutFilter(rightLowCut, cutCoefficients, chainSet.lowCutSlope);
}

void EQuatorAudioProcessor::updateHighCutFilters(const chainSettings& chainSet)
{
    auto highCutCoefficients = makeHighCutFilter(chainSet, getSampleRate());

    auto& leftHighCut = leftChain.get<chainPositions::highCut>();
    auto& rightHighCut = rightChain.get<chainPositions::highCut>();

    leftChain.setBypassed<chainPositions::highCut>(chainSet.highCutBypassed);
    rightChain.setBypassed<chainPositions::highCut>(chainSet.highCutBypassed);

    updateCutFilter(leftHighCut, highCutCoefficients, chainSet.highCutSlope);
    updateCutFilter(rightHighCut, highCutCoefficients, chainSet.highCutSlope);
}

void EQuatorAudioProcessor::updateFilters()
{
    auto chainSet = getChainSettings(apvts);

    updateLowCutFilters(chainSet);
    updatePeakFilter(chainSet);
    updateHighCutFilters(chainSet);
}


juce::AudioProcessorValueTreeState::ParameterLayout EQuatorAudioProcessor::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;

    juce::StringArray cutChoices;
    for (int i = 0; i < 4; i++)
    {
        juce::String str;
        str << (12 + i * 12);
        str << " dB/Oct";
        cutChoices.add(str);
    }

 
    layout.add(std::make_unique<juce::AudioParameterBool>("LOWCUT BYPASS", "LowCut Bypass", false));
    layout.add(std::make_unique<juce::AudioParameterBool>("PEAK BYPASS", "Peak Bypass", false));
    layout.add(std::make_unique<juce::AudioParameterBool>("HIGHCUT BYPASS", "HighCut Bypass", false));


    // Band 1 (Low)
    layout.add(std::make_unique<juce::AudioParameterFloat>
        ("LOWCUT FREQ", "LowCut Freq",
            juce::NormalisableRange<float>(20.0f, 20000.0f, 1.0f, 0.25f), 20.0f));
                                        //(MinValue, MaxValue, Increment, SkewFactor).

    layout.add(std::make_unique<juce::AudioParameterChoice>
        ("LOWCUT SLOPE", "LowCut Slope", cutChoices, 0)); // default choice = 0.


    // Band 2 (Mid)
    layout.add(std::make_unique<juce::AudioParameterFloat>
        ("PEAK FREQ", "Peak Freq",
            juce::NormalisableRange<float>(20.0f, 20000.0f, 1.0f, 0.25f), 750.0f));

    layout.add(std::make_unique<juce::AudioParameterFloat>
        ("PEAK GAIN", "Peak Gain",
            juce::NormalisableRange<float>(-24.0f, 24.0f, 1.0f, 1.0f), 0.0f));

    layout.add(std::make_unique<juce::AudioParameterFloat>
        ("PEAK QUALITY", "Peak Quality",
            juce::NormalisableRange<float>(0.1f, 10.0f, 0.05f, 1.0f), 1.0f));


    // Band 3 (High)
    layout.add(std::make_unique<juce::AudioParameterFloat>
        ("HIGHCUT FREQ", "HighCut Freq",
            juce::NormalisableRange<float>(20.0f, 20000.0f, 1.0f, 0.25f), 20000.0f));

    layout.add(std::make_unique<juce::AudioParameterChoice>
        ("HIGHCUT SLOPE", "HighCut Slope", cutChoices, 0)); // default choice = 0.


    return layout;
}

//==============================================================================

EQuatorAudioProcessor::EQuatorAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

EQuatorAudioProcessor::~EQuatorAudioProcessor()
{
}

//==============================================================================

const juce::String EQuatorAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool EQuatorAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool EQuatorAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool EQuatorAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double EQuatorAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int EQuatorAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int EQuatorAudioProcessor::getCurrentProgram()
{
    return 0;
}

void EQuatorAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String EQuatorAudioProcessor::getProgramName (int index)
{
    return {};
}

void EQuatorAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================

void EQuatorAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    juce::dsp::ProcessSpec spec;

    spec.sampleRate = sampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = 1;

    leftChain.prepare(spec);
    rightChain.prepare(spec);

    updateFilters();

    //leftChannelFifo.prepare(samplesPerBlock);
    //rightChannelFifo.prepare(samplesPerBlock);
}

void EQuatorAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool EQuatorAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void EQuatorAudioProcessor::processBlock(juce::AudioBuffer<float>& buffer, 
                                         juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear(i, 0, buffer.getNumSamples());


    updateFilters();


    juce::dsp::AudioBlock<float> block(buffer);

    juce::dsp::AudioBlock<float> leftBlock = block.getSingleChannelBlock(0);
    juce::dsp::AudioBlock<float> rightBlock = block.getSingleChannelBlock(1);

    juce::dsp::ProcessContextReplacing<float> leftContext(leftBlock);
    juce::dsp::ProcessContextReplacing<float> rightContext(rightBlock);

    leftChain.process(leftContext);
    rightChain.process(rightContext);

    //leftChannelFifo.update(buffer);
    //rightChannelFifo.update(buffer);
}

//==============================================================================

bool EQuatorAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* EQuatorAudioProcessor::createEditor()
{
    return new EQuatorAudioProcessorEditor (*this);
    //return new juce::GenericAudioProcessorEditor(*this);
}

//==============================================================================

void EQuatorAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    juce::MemoryOutputStream mos(destData, true);
    apvts.state.writeToStream(mos);
}

void EQuatorAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    if (tree.isValid())
    {
        apvts.replaceState(tree);
        updateFilters();
    }
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new EQuatorAudioProcessor();
}
