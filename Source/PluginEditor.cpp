#include "PluginProcessor.h"
#include "PluginEditor.h"


void LookAndFeel::drawRotarySlider(juce::Graphics& g,
    int x, int y, int width, int height,
    float sliderPosProportional,
    float rotaryStartAngle, float rotaryEndAngle,
    juce::Slider& slider)
{
    using namespace juce;

    auto bounds = Rectangle<float>(x, y, width, height);

    auto enabled = slider.isEnabled();

    g.setColour(enabled ? Colours::deepskyblue : Colours::darkgrey);
    g.fillEllipse(bounds);

    g.setColour(enabled ? Colours::blue : Colours::darkblue);
    g.drawEllipse(bounds, 2.0f);

    if (auto* rswl = dynamic_cast<RotarySliderWithLabels*>(&slider))
    {
        auto centre = bounds.getCentre();

        Path p; // needed to be able to rotate r.

        Rectangle<float> r;

        r.setLeft(centre.getX() - 2);
        r.setRight(centre.getX() + 2);
        r.setTop(bounds.getY());
        r.setBottom(centre.getY() - rswl->getTextHeight() * 1.5);

        p.addRoundedRectangle(r, 2.0f);

        jassert(rotaryStartAngle < rotaryEndAngle);

        auto sliderAngRad = jmap(sliderPosProportional, 0.0f, 1.0f, rotaryStartAngle, rotaryEndAngle);

        p.applyTransform(AffineTransform().rotated(sliderAngRad, centre.getX(), centre.getY()));

        g.fillPath(p);

        g.setFont(rswl->getTextHeight());
        auto text = rswl->getDisplayString();
        auto strWidth = g.getCurrentFont().getStringWidth(text);

        r.setSize(strWidth + 4, rswl->getTextHeight() + 2);
        r.setCentre(bounds.getCentre());

        g.setColour(Colours::black);
        g.fillRect(r);

        g.setColour(Colours::white);
        g.drawFittedText(text, r.toNearestInt(), Justification::centred, 1);
    }
}

void LookAndFeel::drawToggleButton( juce::Graphics& g,
                                    juce::ToggleButton& toggleButton,
                                    bool shouldDrawButtonsAsHighlighted,
                                    bool shouldDrawButtonsAsDown)
{
    using namespace juce;

    Path powerButton;

    auto bounds = toggleButton.getLocalBounds();
    auto size = jmin(bounds.getWidth(), bounds.getHeight() - 6);
    auto r = bounds.withSizeKeepingCentre(size, size).toFloat();

    float ang = 30.0f;
    size -= 6;

    powerButton.addCentredArc(  r.getCentreX(),
                                r.getCentreY(),
                                size * 0.5, size * 0.5,
                                0.0f,
                                degreesToRadians(ang),
                                degreesToRadians(360.0f - ang),
                                true);

    powerButton.startNewSubPath(r.getCentreX(), r.getY());
    powerButton.lineTo(r.getCentre());

    PathStrokeType pst(2.f, PathStrokeType::JointStyle::curved);

    auto colour = toggleButton.getToggleState() ? Colours::darkgrey : Colours::deepskyblue;

    g.setColour(colour);
    g.strokePath(powerButton, pst);
    g.drawEllipse(r, 2.0f);
}

void RotarySliderWithLabels::paint(juce::Graphics& g)
{
    using namespace juce;

    auto startAngle = degreesToRadians(180.0f + 45.0f);
    auto endAngle = degreesToRadians(180.0f - 45.0f) + MathConstants<float>::twoPi;

    auto range = getRange();

    auto sliderBounds = getSliderBounds();

    //g.setColour(Colours::red);
    //g.drawRect(getLocalBounds());
    //
    //g.setColour(Colours::yellow);
    //g.drawRect(sliderBounds);

    getLookAndFeel().drawRotarySlider(  g,
                                        sliderBounds.getX(),
                                        sliderBounds.getY(),
                                        sliderBounds.getWidth(),
                                        sliderBounds.getHeight(),
                                        jmap(getValue(), range.getStart(), range.getEnd(), 0.0, 1.0), 
                                        startAngle, 
                                        endAngle,
                                        *this );
    
    auto centre = sliderBounds.toFloat().getCentre();
    auto radius = sliderBounds.getWidth() * 0.5f;

    g.setColour(Colours::lightblue);
    g.setFont(getTextHeight() - 2.0f);

    auto numChoices = labels.size();
    for (int i = 0; i < numChoices; i++)
    {
        auto pos = labels[i].pos;
        jassert(0.0f <= pos);
        jassert(pos <= 1.0f);

        auto ang = jmap(pos, 0.0f, 1.0f, startAngle, endAngle);

        auto c = centre.getPointOnCircumference(radius + getTextHeight() * 0.5f + 1, ang);

        Rectangle<float> r;
        auto str = labels[i].label;
        r.setSize(g.getCurrentFont().getStringWidth(str), getTextHeight());
        r.setCentre(c);
        r.setY(r.getY() + getTextHeight());

        g.drawFittedText(str, r.toNearestInt(), juce::Justification::centred, 1);
    }

    
}

juce::Rectangle<int> RotarySliderWithLabels::getSliderBounds() const
{
    auto bounds = getLocalBounds();

    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight());

    size -= getTextHeight() * 2;
    juce::Rectangle<int> r;
    r.setSize(size, size);
    r.setCentre(bounds.getCentreX(), 0);
    r.setY(2);

    return r;
}

juce::String RotarySliderWithLabels::getDisplayString() const
{
    if (auto* choiceParam = dynamic_cast<juce::AudioParameterChoice*>(param))
        return choiceParam->getCurrentChoiceName();

    juce::String str;
    bool addK = false;

    if (auto* floatParam = dynamic_cast<juce::AudioParameterFloat*>(param))
    {
        float val = getValue();

        if (val > 999.0f)
        {
            val /= 1000.0f;
            addK = true;
        }

        str = juce::String(val, (addK ? 2 : 0));
    }
    else
    {
        jassertfalse; // shouldn't happen.
    }

    if (suffix.isNotEmpty())
    {
        str << " ";
        if (addK)
            str << "k";
        str << suffix;
    }

    return str;
}

//==============================================================================

ResponseCurveComponent::ResponseCurveComponent(EQuatorAudioProcessor& p) : 
    audioProcessor(p)
    //leftChannelFifo(&audioProcessor.leftChannelFifo)
{
    const auto& params = audioProcessor.getParameters();
    for (auto param : params)
    {
        param->addListener(this);
    }

    // Spectrum analyzer code (faulty)
    /*
    leftChannelFFTDataGenerator.changeOrder(FFTOrder::order2048);   // 48000 / 2048 = 23 Hz per bin.
    monoBuffer.setSize(1, leftChannelFFTDataGenerator.getFFTSize());
    */

    updateChain();

    startTimerHz(60);
}

ResponseCurveComponent::~ResponseCurveComponent()
{
    const auto& params = audioProcessor.getParameters();
    for (auto param : params)
    {
        param->removeListener(this);
    }
}

void ResponseCurveComponent::paint(juce::Graphics& g)
{
    using namespace juce;
    g.fillAll(Colours::black);

    // Draw stars (faulty)
    /*
    Random ran;
    auto starBounds = getLocalBounds();
    float sepWindow = JUCE_LIVE_CONSTANT(2);
    g.setColour(Colours::lightyellow);

    for (int x = 0; x < starBounds.getWidth(); x + sepWindow)
    {
        for (int y = 0; y < starBounds.getHeight(); y + sepWindow)
        {
            if (ran.nextBool())
            {
                g.setOpacity(ran.nextFloat());
                g.drawEllipse(x, y, 2.0f, 2.0f, 1.0f);
            }
        }
    }
    */


    // Draw frequency response curve: ==============================================

    auto responseArea = getLocalBounds();
    auto width = responseArea.getWidth();

    g.setOpacity(.4f);
    g.drawImage(background, responseArea.toFloat());

    auto& lowcut = monoChain.get<chainPositions::lowCut>();
    auto& peak = monoChain.get<chainPositions::peak>();
    auto& highcut = monoChain.get<chainPositions::highCut>();

    auto sampleRate = audioProcessor.getSampleRate();

    std::vector<double> magnitudes;     // Place to store the magnitudes of each frequency.

    magnitudes.resize(width);

    // map width pixels to log10 20-20k scale, then for each pixel get magnitude in dB of that freq
    // and save all as a vector of size width in dB.
    for (int i = 0; i < width; i++)
    {
        double mag = 1.0f;
        auto freq = mapToLog10(double(i) / double(width), 20.0, 20000.0);

        if (!monoChain.isBypassed<chainPositions::peak>())
            mag *= peak.coefficients->getMagnitudeForFrequency(freq, sampleRate);

        if (!monoChain.isBypassed<chainPositions::lowCut>())
        {
            if (!lowcut.isBypassed<0>())
                mag *= lowcut.get<0>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!lowcut.isBypassed<1>())
                mag *= lowcut.get<1>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!lowcut.isBypassed<2>())
                mag *= lowcut.get<2>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!lowcut.isBypassed<3>())
                mag *= lowcut.get<3>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        }

        if (!monoChain.isBypassed<chainPositions::highCut>())
        {
            if (!highcut.isBypassed<0>())
                mag *= highcut.get<0>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!highcut.isBypassed<1>())
                mag *= highcut.get<1>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!highcut.isBypassed<2>())
                mag *= highcut.get<2>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
            if (!highcut.isBypassed<3>())
                mag *= highcut.get<3>().coefficients->getMagnitudeForFrequency(freq, sampleRate);
        }

        magnitudes[i] = Decibels::gainToDecibels(mag);
    }

    Path responseCurve;

    const double outputMin = responseArea.getBottom();
    const double outputMax = responseArea.getY();
    auto map = [outputMin, outputMax](double input)
    {
        return jmap(input, -24.0, 24.0, outputMin, outputMax);
    };

    responseCurve.startNewSubPath(responseArea.getX(), map(magnitudes.front()));

    for (size_t i = 1; i < magnitudes.size(); ++i)
    {
        responseCurve.lineTo(responseArea.getX() + i, map(magnitudes[i]));
    }

    g.setColour(Colours::blue);
    g.setOpacity(0.2f);
    g.fillEllipse(responseArea.toFloat());

    g.setColour(Colours::skyblue);
    //g.drawRoundedRectangle(responseArea.toFloat(), 100.0f, 1.0f);
    g.drawEllipse(responseArea.toFloat(), 1.0f);

    g.setColour(Colours::red);
    g.strokePath(responseCurve, PathStrokeType(2.0f));

    //==============================================================================

}

void ResponseCurveComponent::resized()
{
    using namespace juce;
    background = Image(Image::PixelFormat::RGB, getWidth(), getHeight(), true);

    Graphics g(background);

    int divisions = 10; // number of vertical arcs in globe.

    // Vertical lines (frequency)

    Array<float> freqs;
//    {
//        20, /* 30, 40,*/ 50, 100,
//        200, /*300, 400,*/ 500, 1000,
//        2000, /*3000, 4000,*/ 5000, 10000,
//        20000
//    };
    float fr = 20000.0;
    for (int i = 0; i < divisions; ++i)
    {
        fr /= 2.0;
        freqs.add(fr);
    }

    Array<float> xPos;

    g.setColour(Colours::deepskyblue);
    for (auto f : freqs)
    {
        auto normX = mapFromLog10(f, 20.0f, 20000.0f);
        //g.drawVerticalLine(getWidth() * normX, 0.0f, getHeight());

        auto remove = getWidth() - getWidth() * normX;
        auto newWidth = getWidth() - remove * 2.0;
        Rectangle<float> ellipseArea = getLocalBounds().withSizeKeepingCentre(newWidth, getHeight()).toFloat();

        //g.drawRect(ellipseArea);
        g.drawEllipse(ellipseArea, 1.0f);

        xPos.add(getWidth() * normX);
    }

    // Horizontal lines (gain)
    Array<float> gain
    {
        -24, -18, -12, -6, 0, 6, 12, 18, 24
    };

    Array<float> yPos;

    for (auto gdB : gain)
    {
        auto y = jmap(gdB, -24.0f, 24.0f, float(getHeight()), 0.0f);
        g.setColour(gdB == 0.0f ? Colours::red : Colours::deepskyblue);
        g.drawHorizontalLine(y, 0, getWidth());

        yPos.add(y);
    }

    g.setColour(Colours::white);
    const int fontHeight = 10;
    g.setFont(fontHeight);

    for (int i = 0; i < freqs.size(); i++)
    {
        auto x = xPos[i];

        bool addK = false;
        String str;

        if (freqs[i] > 999.0f)
        {
            addK = true;
            freqs.set(i, freqs[i] / 1000.0f);
        }
        else freqs.set(i, roundFloatToInt(freqs[i]));

        str << freqs[i];
        if (addK)
            str << "k";
        str << "Hz";

        auto textWidth = g.getCurrentFont().getStringWidth(str);
        Rectangle<int> r;

        r.setSize(textWidth, fontHeight);
        r.setCentre(x, 0);
        r.setY(1);

        g.drawFittedText(str, r, Justification::centred, 1);
    }

    for (int i = 0; i < gain.size(); i++)
    {
        auto y = yPos[i];

        String str;

        if (gain[i] > 0)
            str << "+";
        str << gain[i];

        auto textWidth = g.getCurrentFont().getStringWidth(str);
        Rectangle<int> r;

        r.setSize(textWidth, fontHeight);
        r.setX(getWidth() - textWidth);
        r.setCentre(r.getCentreX(), y);

        g.setColour(gain[i] == 0.0f ? Colours::red : Colours::white);

        g.drawFittedText(str, r, Justification::centred, 1);
    }
}


void ResponseCurveComponent::parameterValueChanged(int parameterIndex, float newValue)
{
    parametersChanged.set(true);
}

void ResponseCurveComponent::updateChain()
{
    auto chainSet = getChainSettings(audioProcessor.apvts);

    monoChain.setBypassed<chainPositions::lowCut>(chainSet.lowCutBypassed);
    monoChain.setBypassed<chainPositions::peak>(chainSet.peakBypassed);
    monoChain.setBypassed<chainPositions::highCut>(chainSet.highCutBypassed);

    auto peakCoefficients = makePeakFilter(chainSet, audioProcessor.getSampleRate());
    updateCoefficients(monoChain.get<chainPositions::peak>().coefficients, peakCoefficients);

    auto lowCutCoefficients = makeLowCutFilter(chainSet, audioProcessor.getSampleRate());
    auto highCutCoefficients = makeHighCutFilter(chainSet, audioProcessor.getSampleRate());

    updateCutFilter(monoChain.get<chainPositions::lowCut>(), lowCutCoefficients, chainSet.lowCutSlope);
    updateCutFilter(monoChain.get<chainPositions::highCut>(), highCutCoefficients, chainSet.highCutSlope);
}

void ResponseCurveComponent::timerCallback()
{
    // Spectrum analyzer code (faulty)
    /*
    juce::AudioBuffer<float> tempIncomingBuffer;

    while (leftChannelFifo->getNumCompleteBuffersAvailable() > 0)
    {
        if (leftChannelFifo->getAudioBuffer(tempIncomingBuffer))
        {
            auto size = tempIncomingBuffer.getNumSamples();

            juce::FloatVectorOperations::copy(  monoBuffer.getWritePointer(0, 0),
                                                monoBuffer.getReadPointer(0, size),
                                                monoBuffer.getNumSamples() - size);

            juce::FloatVectorOperations::copy(  monoBuffer.getWritePointer(0, monoBuffer.getNumSamples() - size),
                                                tempIncomingBuffer.getReadPointer(0, 0), 
                                                size);

            leftChannelFFTDataGenerator.produceFFTDataForRendering(monoBuffer, -48.0f);
        }
    }
    */

    if (parametersChanged.compareAndSetBool(false, true))
    {
        updateChain();
        repaint();
    }
}

//==============================================================================

EQuatorAudioProcessorEditor::EQuatorAudioProcessorEditor(EQuatorAudioProcessor& p)
    : AudioProcessorEditor(&p), audioProcessor(p),

    lowCutFreqSlider(*audioProcessor.apvts.getParameter("LOWCUT FREQ"), "Hz"),
    lowCutSlopeSlider(*audioProcessor.apvts.getParameter("LOWCUT SLOPE"), "dB/Oct"),
    peakFreqSlider(*audioProcessor.apvts.getParameter("PEAK FREQ"), "Hz"),
    peakGainSlider(*audioProcessor.apvts.getParameter("PEAK GAIN"), "dB"),
    peakQualitySlider(*audioProcessor.apvts.getParameter("PEAK QUALITY"), ""),
    highCutFreqSlider(*audioProcessor.apvts.getParameter("HIGHCUT FREQ"), "Hz"),
    highCutSlopeSlider(*audioProcessor.apvts.getParameter("HIGHCUT SLOPE"), "dB/Oct"),

    responseCurveComponent(audioProcessor),
    lowCutFreqSliderAttachment(audioProcessor.apvts, "LOWCUT FREQ", lowCutFreqSlider),
    lowCutSlopeSliderAttachment(audioProcessor.apvts,"LOWCUT SLOPE", lowCutSlopeSlider),
    peakFreqSliderAttachment(audioProcessor.apvts, "PEAK FREQ", peakFreqSlider),
    peakGainSliderAttachment(audioProcessor.apvts, "PEAK GAIN", peakGainSlider),
    peakQualitySliderAttachment(audioProcessor.apvts, "PEAK QUALITY", peakQualitySlider),
    highCutFreqSliderAttachment(audioProcessor.apvts, "HIGHCUT FREQ", highCutFreqSlider),
    highCutSlopeSliderAttachment(audioProcessor.apvts, "HIGHCUT SLOPE", highCutSlopeSlider),
    lowCutBypassButtonAttachment(audioProcessor.apvts, "LOWCUT BYPASS", lowCutBypassButton),
    peakBypassButtonAttachment(audioProcessor.apvts, "PEAK BYPASS", peakBypassButton),
    highCutBypassButtonAttachment(audioProcessor.apvts, "HIGHCUT BYPASS", highCutBypassButton)
{
    for (auto* comp : getComps())
    {
        lowCutFreqSlider.labels.add({ 0.0f, "20Hz" });
        lowCutFreqSlider.labels.add({ 1.0f, "20kHz" });

        lowCutSlopeSlider.labels.add({ 0.0f, "12" });
        lowCutSlopeSlider.labels.add({ 1.0f, "48" });

        peakFreqSlider.labels.add({ 0.0f, "20Hz" });
        peakFreqSlider.labels.add({ 1.0f, "20kHz" });

        peakGainSlider.labels.add({ 0.0f, "-24dB" });
        peakGainSlider.labels.add({ 1.0f, "+24dB" });

        peakQualitySlider.labels.add({ 0.0f, "0.1" });
        peakQualitySlider.labels.add({ 1.0f, "10.0" });

        highCutFreqSlider.labels.add({ 0.0f, "20Hz" });
        highCutFreqSlider.labels.add({ 1.0f, "20kHz" });

        highCutSlopeSlider.labels.add({ 0.0f, "12" });
        highCutSlopeSlider.labels.add({ 1.0f, "48" });

        addAndMakeVisible(comp);
    }

    lowCutBypassButton.setLookAndFeel(&lnf);
    peakBypassButton.setLookAndFeel(&lnf);
    highCutBypassButton.setLookAndFeel(&lnf);

    auto safePtr = juce::Component::SafePointer<EQuatorAudioProcessorEditor>(this);

    lowCutBypassButton.onClick = [safePtr]()
    {
        if (auto* comp = safePtr.getComponent())
        {
            auto bypassed = comp->lowCutBypassButton.getToggleState();

            comp->lowCutFreqSlider.setEnabled(!bypassed);
            comp->lowCutSlopeSlider.setEnabled(!bypassed);
        }
    };

    peakBypassButton.onClick = [safePtr]()
    {
        if (auto* comp = safePtr.getComponent())
        {
            auto bypassed = comp->peakBypassButton.getToggleState();

            comp->peakFreqSlider.setEnabled(!bypassed);
            comp->peakGainSlider.setEnabled(!bypassed);
            comp->peakQualitySlider.setEnabled(!bypassed);
        }
    };

    highCutBypassButton.onClick = [safePtr]()
    {
        if (auto* comp = safePtr.getComponent())
        {
            auto bypassed = comp->highCutBypassButton.getToggleState();

            comp->highCutFreqSlider.setEnabled(!bypassed);
            comp->highCutSlopeSlider.setEnabled(!bypassed);
        }
    };

    setSize (400, 500);
}

EQuatorAudioProcessorEditor::~EQuatorAudioProcessorEditor()
{
    lowCutBypassButton.setLookAndFeel(nullptr);
    peakBypassButton.setLookAndFeel(nullptr);
    highCutBypassButton.setLookAndFeel(nullptr);
}

//==============================================================================

void EQuatorAudioProcessorEditor::paint (juce::Graphics& g)
{
    using namespace juce;

    g.fillAll (Colours::black);

}

//==============================================================================

void EQuatorAudioProcessorEditor::resized()
{
    auto bounds = getLocalBounds();
    float hRatio = 40 / 100.f; //JUCE_LIVE_CONSTANT(33) / 100.0f;
    auto responseArea = bounds.removeFromTop(bounds.getHeight() * hRatio);

    responseArea.removeFromTop(20);
    responseArea.removeFromLeft(20);
    responseArea.removeFromRight(20);

    responseCurveComponent.setBounds(responseArea);

    bounds.removeFromTop(10);

    auto lowCutArea = bounds.removeFromLeft(bounds.getWidth() * 0.33);
    auto highCutArea = bounds.removeFromRight(bounds.getWidth() * 0.5);

    lowCutBypassButton.setBounds(lowCutArea.removeFromTop(25));
    lowCutFreqSlider.setBounds(lowCutArea.removeFromTop(lowCutArea.getHeight() * 0.5));
    lowCutSlopeSlider.setBounds(lowCutArea);

    highCutBypassButton.setBounds(highCutArea.removeFromTop(25));
    highCutFreqSlider.setBounds(highCutArea.removeFromTop(highCutArea.getHeight() * 0.5));
    highCutSlopeSlider.setBounds(highCutArea);
    
    peakBypassButton.setBounds(bounds.removeFromTop(25));
    peakFreqSlider.setBounds(bounds.removeFromTop(bounds.getHeight() * 0.33));
    peakGainSlider.setBounds(bounds.removeFromTop(bounds.getHeight() * 0.5));
    peakQualitySlider.setBounds(bounds);
}

std::vector<juce::Component*> EQuatorAudioProcessorEditor::getComps()
{
    return
    {
        &lowCutFreqSlider,
        &lowCutSlopeSlider,
        &peakFreqSlider,
        &peakGainSlider,
        &peakQualitySlider,
        &highCutFreqSlider,
        &highCutSlopeSlider,
        &responseCurveComponent,
        &lowCutBypassButton,
        &peakBypassButton,
        &highCutBypassButton
    };
}