#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

// Spectrum analyzer code (faulty)
/*
enum FFTOrder
{
    order2048 = 11,
    order4096 = 12,
    order8192 = 13
};

template<typename BlockType>
struct FFTDataGenerator
{
    // produces the FFT data from an audio buffer.
    void produceFFTDataForRendering(const juce::AudioBuffer<float>& audioData, const float negativeInfinity)
    {
        const auto fftSize = getFFTSize();

        fftData.assign(fftData.size(), 0);
        auto* readIndex = audioData.getReadPointer(0);
        std::copy(readIndex, readIndex + fftSize, fftData.begin());

        // first apply a winfowing function to our data
        window->multiplyWithWindowingTable(fftData.data(), fftSize);        // [1]

        // then render our FFT data...
        forwardFFT->performFrequencyOnlyForwardTransform(fftData.data());   // [2]

        int numBins = (int)fftSize / 2;

        // normalize the fft values.
        for (int i = 0; i < numBins; ++i)
        {
            fftData[i] /= (float)numBins;
        }

        // convert them to decibels
        for (int i = 0; i < numBins; ++i)
        {
            fftData[i] = juce::Decibels::gainToDecibels(fftData[i], negativeInfinity);
        }

        fftDataFifo.push(fftData);
    }

    void changeOrder(FFTOrder newOrder)
    {
        // when you change order, recreate the window, forwardFFT, fifo, fftData
        // also reset the fifoIndex
        // things that need recreating should be created on the heap via std::make_unique<>

        order = newOrder;
        auto fftSize = getFFTSize();

        forwardFFT = std::make_unique<juce::dsp::FFT>(order);
        window = std::make_unique<juce::dsp::WindowingFunction<float>>(fftSize, juce::dsp::WindowingFunction<float>::blackmanHarris);

        fftData.clear();
        fftData.resize(fftSize * 2, 0);

        fftDataFifo.prepare(fftData.size());
    }
    //==============================================================================
    int getFFTSize() const { return 1 << order; }
    int getNumAvailableFFTDataBlocks() const { return fftDataFifo.getNumAvailableForReading(); }
    //==============================================================================
    bool getFFTData(BlockType& fftData) { return fftDataFifo.pull(fftData); }
private:
    FFTOrder order;
    BlockType fftData;
    std::unique_ptr<juce::dsp::FFT> forwardFFT;
    std::unique_ptr<juce::dsp::WindowingFunction<float>> window;

    Fifo<BlockType> ffTDataFifo;
};
*/

struct LookAndFeel : juce::LookAndFeel_V4
{
    void drawRotarySlider(  juce::Graphics&,
                            int x, int y, int width, int height,
                            float sliderPosProportional,
                            float rotaryStartAngle,
                            float rotaryEndAngle,
                            juce::Slider&) override;

    void drawToggleButton(  juce::Graphics& g,
                            juce::ToggleButton& toggleButton,
                            bool shouldDrawButtonsAsHighlighted,
                            bool shouldDrawButtonsAsDown) override;

};

struct RotarySliderWithLabels : juce::Slider
{
    RotarySliderWithLabels( juce::RangedAudioParameter& rap, const juce::String& unitSuffix ) : 
    juce::Slider(   juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag,
                    juce::Slider::TextEntryBoxPosition::NoTextBox),
    param(&rap),
    suffix(unitSuffix)
    {
        setLookAndFeel(&lnf);
    }

    ~RotarySliderWithLabels()
    {
        setLookAndFeel(nullptr);
    }

    struct labelPos
    {
        float pos;
        juce::String label;
    };

    juce::Array<labelPos> labels;

    void paint(juce::Graphics& g) override;
    juce::Rectangle<int> getSliderBounds() const;
    int getTextHeight() const { return 14; }
    juce::String getDisplayString() const;

private:
    LookAndFeel lnf;

    juce::RangedAudioParameter* param;
    juce::String suffix;

};

struct ResponseCurveComponent : juce::Component,
                                juce::AudioProcessorParameter::Listener,
                                juce::Timer
{
    ResponseCurveComponent(EQuatorAudioProcessor&);
    ~ResponseCurveComponent();

    void parameterValueChanged(int parameterIndex, float newValue) override;

    void parameterGestureChanged(int parameterIndex, bool gestureIsStarting) override {};

    void timerCallback() override;

    void paint(juce::Graphics& g) override;

    void resized() override;

private:
    EQuatorAudioProcessor& audioProcessor;
    juce::Atomic<bool> parametersChanged{ false };

    monoChain monoChain;

    void updateChain();

    juce::Image background;

    //SingleChannelSampleFifo<EQuatorAudioProcessor::BlockType>* leftChannelFifo;
    //SingleChannelSampleFifo<EQuatorAudioProcessor::BlockType>* rightChannelFifo;

    juce::AudioBuffer<float> monoBuffer;

    //FFTDataGenerator<std::vector<float>> leftChannelFFTDataGenerator;
};

//==============================================================================

class EQuatorAudioProcessorEditor : public  juce::AudioProcessorEditor
{
public:
    EQuatorAudioProcessorEditor (EQuatorAudioProcessor&);
    ~EQuatorAudioProcessorEditor() override;

    void paint (juce::Graphics&) override;
    void resized() override;

private:
    EQuatorAudioProcessor& audioProcessor;

    RotarySliderWithLabels  lowCutFreqSlider,
                            lowCutSlopeSlider,
                            peakFreqSlider,
                            peakGainSlider,
                            peakQualitySlider,
                            highCutFreqSlider,
                            highCutSlopeSlider;

    using APVTS = juce::AudioProcessorValueTreeState;
    using Attachment = APVTS::SliderAttachment;

    ResponseCurveComponent responseCurveComponent;

    Attachment  lowCutFreqSliderAttachment,
                lowCutSlopeSliderAttachment,
                peakFreqSliderAttachment,
                peakGainSliderAttachment,
                peakQualitySliderAttachment,
                highCutFreqSliderAttachment,
                highCutSlopeSliderAttachment;

    juce::ToggleButton  lowCutBypassButton, 
                        peakBypassButton, 
                        highCutBypassButton;

    using ButtonAttachment = APVTS::ButtonAttachment;
    ButtonAttachment    lowCutBypassButtonAttachment,
                        peakBypassButtonAttachment,
                        highCutBypassButtonAttachment;

    std::vector<juce::Component*> getComps();

    LookAndFeel lnf;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (EQuatorAudioProcessorEditor)
};
